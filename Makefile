CXX?=g++

HTTMOJO_CPPFLAGS:=-I$(TARTSYS)/include
HTTMOJO_CFLAGS:=-ggdb $(shell root-config --cflags) -std=c++0x
HTTMOJO_LDFLAGS:=-L$(TARTSYS)/lib
HTTMOJO_LIBS:=$(shell root-config --libs) -lanabrips -lanasamurai -lanaroot -lanacore -lXMLParser $(shell pkg-config freetype2 --libs) -lrt

PLOTTER_CPPFLAGS:=
PLOTTER_CFLAGS:=-ggdb $(shell root-config --cflags) -std=c++0x
PLOTTER_LDFLAGS:=
PLOTTER_LIBS:=$(shell root-config --libs) $(shell pkg-config freetype2 --libs)

.PHONY: all
all: plotter toyer

plotter: plotter.o
	$(CXX) -o $@ $^ $(PLOTTER_LIBS)

plotter.o: plotter.cc Makefile
	$(CXX) -c -o $@ $< $(PLOTTER_CPPFLAGS) $(PLOTTER_CFLAGS)

toyer: plotter
	ln -sf $< $@

httmojo: httmojo.o httmojo_dict.o
	$(CXX) -o $@ $^ $(HTTMOJO_LDFLAGS) $(HTTMOJO_LIBS)

httmoj%.o: httmoj%.cc Makefile
	$(CXX) -c -o $@ $< $(HTTMOJO_CPPFLAGS) $(HTTMOJO_CFLAGS)

httmojo_dict.cc: httmojo.hh Makefile
	rootcint -f $@ -c $(HTTMOJO_CPPFLAGS) $<
	sed -i 's,\[6\],,' $@

.PHONY: clean
clean:
	rm -f *.o *_C.* httmojo httmojo_dict* plotter
