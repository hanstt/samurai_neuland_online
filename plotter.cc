#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/stat.h>
#include <algorithm>
#include <cassert>
#include <cerrno>
#include <csignal>
#include <err.h>
#include <cstdint>
#include <cstdio>
#include <ctime>
#include <fstream>
#include <iostream>
#include <list>
#include <map>
#include <sstream>
#include <TBox.h>
#include <TCanvas.h>
#include <TDatime.h>
#include <TGraph.h>
#include <TH1I.h>
#include <TH2I.h>
#include <TList.h>
#include <TMultiGraph.h>
#include <TMutex.h>
#include <TPaveStats.h>
#include <TSystem.h>

#define int32_t Int_t
#define LENGTH(x) (sizeof x / sizeof x[0])
#define LOC "\n" << __FILE__":" << __LINE__ << ": "

namespace {

  struct Graph;
  struct H1I;
  struct H2I;
  struct MultiGraph;
  struct Ref {
    enum Type {
      kH1I,
      kH2I,
      kMultiGraph
    } type;
    union Obj {
      Obj(H1I *h1i): h1i(h1i) {}
      Obj(H2I *h2i): h2i(h2i) {}
      Obj(MultiGraph *mg): mg(mg) {}
      H1I *h1i;
      H2I *h2i;
      MultiGraph *mg;
    } obj;
    Ref(Type type, H1I *h1i): type(type), obj(h1i) {}
    Ref(Type type, H2I *h2i): type(type), obj(h2i) {}
    Ref(Type type, MultiGraph *mg): type(type), obj(mg) {}
  };
  struct Graph {
    Graph(): touched(), max(), title(), tgraph(new TGraph) {}
    ~Graph() { delete tgraph; }
    bool touched;
    int max;
    std::string title;
    TGraph *tgraph;
  };
  struct H1I {
    H1I(char const *name, char const *title, int nx, double xmin, double
        xmax): touched(), th1i(new TH1I(name, title, nx, xmin, xmax)) {}
    ~H1I() { delete th1i; }
    bool touched;
    double xmin, xmax;
    uint8_t logy;
    uint32_t opt_stat;
    TH1I *th1i;
  };
  struct H2I {
    H2I(char const *name, char const *title, int nx, double xmin, double xmax,
        int ny, double ymin, double ymax): touched(), th2i(new TH2I(name,
            title, nx, xmin, xmax, ny, ymin, ymax)) {}
    ~H2I() { delete th2i; }
    bool touched;
    double xmin, xmax;
    double ymin, ymax;
    uint8_t logz;
    uint32_t opt_stat;
    TH2I *th2i;
  };
  struct MultiGraph {
    MultiGraph(): touched(), gridy(), tmg(new TMultiGraph), graph_list() {}
    ~MultiGraph() {
      TList *grlist = tmg->GetListOfGraphs();
      if (grlist) {
        for (;;) {
          TGraph *tg = (TGraph *)grlist->First();
          if (!tg) {
            break;
          }
          tmg->RecursiveRemove(tg);
        }
      }
      delete tmg;
    }
    bool touched;
    uint8_t gridy;
    TMultiGraph *tmg;
    std::vector<Graph *> graph_list;
  };
  struct Canvas {
    Canvas(): touched(), row_num(-1), col_num(-1), tcanvas() {}
    ~Canvas() { delete tcanvas; }
    bool touched;
    int row_num, col_num;
    TCanvas *tcanvas;
    std::vector<Ref> ref_list;
  };

  bool g_is_toyer = false;
  int32_t *g_iarr;
  int g_do_stop = 0;
  char g_time_buf[26];
  uint32_t g_time_dump_s;
  uint64_t g_neve_monitor;
  TMutex *g_fetcher_mutex;
  std::map<std::string, Canvas *> g_canvas_map;
  std::map<std::string, Graph *> g_graph_map;
  std::map<std::string, H1I *> g_h1i_map;
  std::map<std::string, H2I *> g_h2i_map;
  std::map<std::string, MultiGraph *> g_multigraph_map;
  Canvas *g_canvas_prev;
  MultiGraph *g_multigraph_prev;
  int g_name_id;

}

#define READ_PRIMITIVE(var)\
    do {\
      if (1 != fread(&var, sizeof var, 1, a_file)) {\
        std::cerr << LOC << "Primitive " << #var << " size=" << sizeof var <<\
        " could not be read." << std::endl;\
        return false;\
      }\
    } while (0)

void *fetcher(void *)
{
  while (0 == g_do_stop) {
    g_fetcher_mutex->Lock();
#ifdef WIN32
    system("fetcher.bat");
#else
    system("./fetcher.sh");
#endif
    g_fetcher_mutex->UnLock();
    gSystem->Sleep(3000);
  }
  return NULL;
}

char const *my_ctime(time_t const *a_clock)
{
  size_t len;
  char *p;

  strcpy(g_time_buf, ctime(a_clock));
  len = strlen(g_time_buf);
  p = strchr(g_time_buf, '\n');
  if (NULL != p) {
    *p = '\0';
  }
  return g_time_buf;
}

bool isRangeOk(int32_t a_bins, Double_t a_min, Double_t const a_max)
{
  return 0 < a_bins && 1e4 >= a_bins &&
      -1e4 < a_min && 1e4 > a_min &&
      -1e4 < a_max && 1e4 > a_max &&
      a_min < a_max;
}

bool readHeader(FILE *a_file, char *a_header)
{
  int ret = fread(a_header, 4, 1, a_file);
  if (0 == ret && feof(a_file)) {
    a_header[0] = '\0';
    return true;
  }
  if (1 != ret) {
    std::cerr << LOC << "Could not read." << std::endl;
    return false;
  }
  a_header[4] = '\0';
  return true;
}

bool readString(FILE *a_file, std::string *a_str)
{
  int slen = fgetc(a_file);
  if (0 > slen) {
    std::cerr << LOC << "Length invalid (" << slen << ")." << std::endl;
    return false;
  }
  char *str = new char [slen + 1];
  if (1 != fread(str, slen, 1, a_file)) {
    std::cerr << LOC << "Unexpected EOF." << std::endl;
  }
  str[slen] = '\0';
  *a_str = str;
  free(str);
  return true;
}

bool readTCanvas(FILE *a_file)
{
  std::string title;
  if (!readString(a_file, &title)) {
    std::cerr << LOC << "Could not read title." << std::endl;
    return false;
  }
  uint8_t row_num, col_num;
  READ_PRIMITIVE(row_num);
  READ_PRIMITIVE(col_num);

  Canvas *canvas;
  auto it = g_canvas_map.find(title);
  if (g_canvas_map.end() != it) {
    canvas = it->second;
  } else {
    std::ostringstream oss;
    oss << "c" << g_name_id++;
    canvas = new Canvas;
    canvas->tcanvas = new TCanvas(oss.str().c_str(), title.c_str(), 1200,
        700);
    g_canvas_map[title] = canvas;
  }
  canvas->row_num = row_num;
  canvas->col_num = col_num;
  canvas->touched = true;
  g_canvas_prev = canvas;
  return true;
}

bool readTH1I(FILE *a_file)
{
  if (!g_canvas_prev) {
    std::cerr << LOC << "No previous canvas." << std::endl;
    return false;
  }

  std::string title;
  readString(a_file, &title);

  int16_t nx;
  double xmin, xmax;
  READ_PRIMITIVE(nx);
  READ_PRIMITIVE(xmin);
  READ_PRIMITIVE(xmax);
  if (!isRangeOk(nx, xmin, xmax)) {
    std::cerr << LOC << "Size weird (" << nx << ", " << xmin << ", " << xmax
        << ")." << std::endl;
    return false;
  }
  uint8_t logy;
  READ_PRIMITIVE(logy);

  double nentries;
  READ_PRIMITIVE(nentries);
  double stats[4];
  if (LENGTH(stats) != fread(stats, sizeof *stats, LENGTH(stats), a_file)) {
    std::cerr << LOC << "Stats could not be read." << std::endl;
    return false;
  }
  uint32_t opt_stat;
  READ_PRIMITIVE(opt_stat);
  int ret = fread(g_iarr, sizeof *g_iarr, 1 + nx + 1, a_file);
  if (ret != 1 + nx + 1) {
    std::cout << LOC << "fread(1 + (nx=" << nx << ") + 1) = " << ret << "." <<
        std::endl;
    return false;
  }

  /* Set histogram once we have all data. */
  H1I *h1i;
  auto it = g_h1i_map.find(title);
  if (g_h1i_map.end() != it) {
    h1i = it->second;
  } else {
    std::ostringstream oss;
    oss << "h1i" << g_name_id++;
    h1i = new H1I(oss.str().c_str(), title.c_str(), nx, xmin, xmax);
    g_h1i_map[title] = h1i;
  }
  h1i->xmin = xmin;
  h1i->xmax = xmax;
  h1i->logy = logy;
  h1i->opt_stat = opt_stat;
  auto th1i = h1i->th1i;
  if (th1i->GetNbinsX() != nx) {
    th1i->SetBins(nx, xmin, xmax);
  }
  th1i->SetEntries(nentries);
  th1i->PutStats(stats);
  th1i->Set(1 + nx + 1, g_iarr);

  Ref ref(Ref::kH1I, h1i);
  g_canvas_prev->ref_list.push_back(ref);

  h1i->touched = true;
  return true;
}

bool readTH2I(FILE *a_file)
{
  if (!g_canvas_prev) {
    std::cerr << LOC << "No previous canvas." << std::endl;
    return false;
  }

  std::string title;
  readString(a_file, &title);

  int16_t nx, ny;
  double xmin, xmax, ymin, ymax;
  READ_PRIMITIVE(nx);
  READ_PRIMITIVE(xmin);
  READ_PRIMITIVE(xmax);
  READ_PRIMITIVE(ny);
  READ_PRIMITIVE(ymin);
  READ_PRIMITIVE(ymax);
  if (!isRangeOk(nx, xmin, xmax) || !isRangeOk(ny, ymin, ymax)) {
    std::cerr << LOC << "Size weird (" << nx << ", " << xmin << ", " << xmax
        << ") x (" << ny << ", " << ymin << ", " << ymax << ")." << std::endl;
    return false;
  }

  uint8_t logz;
  READ_PRIMITIVE(logz);

  double nentries;
  READ_PRIMITIVE(nentries);

  Double_t stats[7];
  if (LENGTH(stats) != fread(stats, sizeof *stats, LENGTH(stats), a_file)) {
    std::cerr << LOC << "Stats could not be read." << std::endl;
    return false;
  }
  uint32_t opt_stat;
  READ_PRIMITIVE(opt_stat);
  int ret = fread(g_iarr, sizeof *g_iarr, (1 + nx + 1) * (1 + ny + 1), a_file);
  if ((1 + nx + 1) * (1 + ny + 1) != ret) {
    std::cout << LOC << "fread[(1 + (nx=" << nx << ") + 1) * (1 + (ny=" << ny
        << ") + 1)] = " << ret << "." << std::endl;
    return false;
  }

  /* Set histogram once we have all data. */
  H2I *h2i;
  auto it = g_h2i_map.find(title);
  if (g_h2i_map.end() != it) {
    h2i = it->second;
  } else {
    std::ostringstream oss;
    oss << "h2i" << g_name_id++;
    h2i = new H2I(oss.str().c_str(), title.c_str(), nx, xmin, xmax, ny, ymin,
        ymax);
    g_h2i_map[title] = h2i;
  }
  h2i->xmin = xmin;
  h2i->xmax = xmax;
  h2i->ymin = ymin;
  h2i->ymax = ymax;
  h2i->logz = logz;
  h2i->opt_stat = opt_stat;
  auto th2i = h2i->th2i;
  if (th2i->GetNbinsX() != nx || th2i->GetNbinsY() != ny) {
    th2i->SetBins(nx, xmin, xmax, ny, ymin, ymax);
  }
  th2i->SetEntries(nentries);
  th2i->PutStats(stats);
  th2i->Set((1 + nx + 1) * (1 + ny + 1), g_iarr);

  Ref ref(Ref::kH2I, h2i);
  g_canvas_prev->ref_list.push_back(ref);

  h2i->touched = true;
  return true;
}

bool readTMultiGraph(FILE *a_file)
{
  if (!g_canvas_prev) {
    std::cerr << LOC << "No previous canvas." << std::endl;
    return false;
  }

  std::string title;
  readString(a_file, &title);

  uint8_t gridy;
  READ_PRIMITIVE(gridy);

  MultiGraph *mg;
  auto it = g_multigraph_map.find(title);
  if (g_multigraph_map.end() != it) {
    mg = it->second;
  } else {
    std::ostringstream oss;
    oss << "mg" << g_name_id++;
    mg = new MultiGraph;
    g_multigraph_map[title] = mg;
  }
  mg->gridy = gridy;

  Ref ref(Ref::kMultiGraph, mg);
  g_canvas_prev->ref_list.push_back(ref);

  g_multigraph_prev = mg;
  mg->touched = true;
  return true;
}

bool readVec32(FILE *a_file)
{
  if (!g_multigraph_prev) {
    std::cerr << LOC << "No previous multigraph." << std::endl;
    return false;
  }

  std::string title;
  readString(a_file, &title);

  uint8_t color;
  READ_PRIMITIVE(color);

  uint32_t num;
  READ_PRIMITIVE(num);
  if (num <= 0 || 100000 < num) {
    std::cerr << LOC << "Size weird (" << num << ")." << std::endl;
    return false;
  }

  int ret = fread(g_iarr, sizeof(int32_t), num, a_file);
  if (num != ret) {
    std::cout << LOC << "fread(num=" << num << ") = " << ret << "." <<
        std::endl;
    return false;
  }

  /* Set vector once we have secured all data. */
  Graph *g;
  auto it = g_graph_map.find(title);
  if (g_graph_map.end() != it) {
    g = it->second;
  } else {
    g = new Graph;
    g->title = title;
    g_graph_map[title] = g;
  }
  int max = 0;
  for (size_t i = 0; num > i; ++i) {
    max = std::max(max, g_iarr[i]);
  }
  g->max = max;
  auto tg = g->tgraph;
  if (tg->GetN() != num) {
    tg->Set(num);
  }
  for (size_t i = 0; num > i; ++i) {
    tg->SetPoint(i, i, g_iarr[i]);
  }
  tg->SetMarkerColor(color);
  tg->SetMarkerStyle(7);

  g_multigraph_prev->graph_list.push_back(g);
  g->touched = true;
  return true;
}

bool parse(int a_size, FILE *a_file)
{
  uint32_t tot_size;
  READ_PRIMITIVE(tot_size);
  if (a_size != tot_size) {
    std::cout << LOC << "Filesize mismatch (expected=" << tot_size <<
        " actual=" << a_size << ").";
    return false;
  }
  READ_PRIMITIVE(g_time_dump_s);
  READ_PRIMITIVE(g_neve_monitor);

  for (;;) {
    char header[5];
    readHeader(a_file, header);
    if ('\0' == header[0]) {
      break;
    }
    if (0 == strcmp(header, "TCvs")) {
      readTCanvas(a_file);
    } else if (0 == strcmp(header, "TH1I")) {
      readTH1I(a_file);
    } else if (0 == strcmp(header, "TH2I")) {
      readTH2I(a_file);
    } else if (0 == strcmp(header, "TMGp")) {
      readTMultiGraph(a_file);
    } else if (0 == strcmp(header, "Vc32")) {
      readVec32(a_file);
    } else {
      std::cerr << LOC << "Corrupt server data." << std::endl;
      return false;
    }
  }
}

void sighandler(int)
{
  puts("\nCaught SIGINT, hold on to your horses because I am about to quit!");
  ++g_do_stop;
  if (3 == g_do_stop) {
    puts("User spamming ctrl+c, so I kill myself now.");
    exit(EXIT_FAILURE);
  }
}

void plotter()
{
  signal(SIGINT, sighandler);

  g_fetcher_mutex = new TMutex();
  TThread *thread = new TThread("thread", fetcher, NULL);
  thread->Run();

  g_iarr = new int32_t [1000 * 1000];

  // neve shm for other processes.
  key_t const c_key = 1007;
  int const c_shmsiz = 5;
  int shmid;
  char *shm;
  if ((shmid = shmget(c_key, c_shmsiz, IPC_CREAT | 0666)) < 0) {
    err(EXIT_FAILURE, "shmget");
  }
  if ((shm = (char *)shmat(shmid, NULL, 0)) == (char *) -1) {
    err(EXIT_FAILURE, "shmat");
  }
  *shm = 0;

  time_t tt_dump = 0;
  while (0 == g_do_stop) {
    gSystem->Sleep(1000);
    time_t tt = time(NULL);

    bool is_data_fine = false;
    g_fetcher_mutex->Lock();
#define DATA_FILENAME "hists.dat"
    bool read_next = true;
    FILE *file = fopen(DATA_FILENAME, "rb");
    if (NULL == file) {
      std::cerr << my_ctime(&tt) << ":fopen(" DATA_FILENAME "): " <<
        strerror(errno) << "." << std::endl;
      goto parsing_done;
    }
    struct stat st;
    stat(DATA_FILENAME, &st);

    /* Untouch all objects. */
#define SET_UNTOUCHED(name)\
    do {\
      for (auto it = g_##name##_map.begin(); g_##name##_map.end() != it; ++it) {\
        it->second->touched = false;\
      }\
    } while (0)
    SET_UNTOUCHED(canvas);
    SET_UNTOUCHED(graph);
    SET_UNTOUCHED(h1i);
    SET_UNTOUCHED(h2i);
    SET_UNTOUCHED(multigraph);
    for (auto it = g_canvas_map.begin(); g_canvas_map.end() != it; ++it) {
      it->second->ref_list.clear();
    }

    /* Parse. */
    g_canvas_prev = nullptr;
    g_multigraph_prev = nullptr;
    parse(st.st_size, file);
    {
      memmove(shm + sizeof(uint32_t), &g_neve_monitor, 8);
      memmove(shm, &tt, sizeof(uint32_t));
    }
    tt_dump = g_time_dump_s;

    /* Remove untouched. */
#define DELETE_UNTOUCHED(name)\
    do {\
      for (auto it = g_##name##_map.begin(); g_##name##_map.end() != it;) {\
        if (it->second->touched) {\
          ++it;\
        } else {\
          delete it->second;\
          g_##name##_map.erase(it++);\
        }\
      }\
    } while (0)
    DELETE_UNTOUCHED(canvas);
    DELETE_UNTOUCHED(graph);
    DELETE_UNTOUCHED(h1i);
    DELETE_UNTOUCHED(h2i);
    DELETE_UNTOUCHED(multigraph);

    /* Update multigraphs. */
    for (auto it = g_multigraph_map.begin(); g_multigraph_map.end() != it;
        ++it) {
      auto *mg = it->second;
      auto &tmg = mg->tmg;
      /* Check if anything changed. */
      bool has_changed = false;
      TIterator *grit = nullptr;
      TList *grlist = tmg->GetListOfGraphs();
      if (grlist) {
        grit = grlist->MakeIterator();
      }
      for (auto it2 = mg->graph_list.begin(); mg->graph_list.end() != it2;
          ++it2) {
        if (!grit ||
            (TGraph *)grit->Next() != (*it2)->tgraph) {
          has_changed = true;
          break;
        }
      }
      if (has_changed) {
        /* Need to rebuild, start by removing all graphs. */
        for (;;) {
          TList *grlist = tmg->GetListOfGraphs();
          if (!grlist) {
            break;
          }
          TGraph *tg = (TGraph *)grlist->First();
          if (!tg) {
            break;
          }
          tmg->RecursiveRemove(tg);
        }
        /* And add all. */
        for (auto it2 = mg->graph_list.begin(); mg->graph_list.end() != it2;
            ++it2) {
          tmg->Add((*it2)->tgraph);
        }
      }
      /* Set max, add 10%. */
      int max = 0;
      for (auto it2 = mg->graph_list.begin(); mg->graph_list.end() != it2;
          ++it2) {
        max = std::max(max, (*it2)->max);
      }
      tmg->SetMaximum(max * 1.1);
    }
    is_data_fine = true;

parsing_done:
    if (file) {
      fclose(file);
    }
    g_fetcher_mutex->UnLock();

    /* Draw. */
    bool is_old_dump = 30 < (tt - tt_dump);
    for (auto it = g_canvas_map.begin(); g_canvas_map.end() != it; ++it) {
      Canvas *c = it->second;
      TCanvas *tc = c->tcanvas;
      tc->Clear();
      TDatime datime;
      char const *lasttime = my_ctime(&tt_dump);
      std::ostringstream oss;
      oss << it->first << " (cur=" << datime.AsString() << ", last=" <<
          lasttime << ")";
      tc->SetTitle(oss.str().c_str());
      tc->Divide(c->col_num, c->row_num);
      tc->SetFillColor(is_old_dump ? 41 : 0);

      int i = 0;
      for (auto it2 = c->ref_list.begin(); c->ref_list.end() != it2; ++it2,
          ++i) {
        TVirtualPad *tpad = tc->cd(1 + i);
        Ref &ref = *it2;
        switch (ref.type) {
          case Ref::kH1I:
            {
              H1I *h1i = ref.obj.h1i;
              if (h1i->logy) {
                tpad->SetLogy();
              }
              h1i->th1i->Draw();
              h1i->th1i->GetXaxis()->SetRangeUser(h1i->xmin, h1i->xmax);
            }
            break;
          case Ref::kH2I:
            {
              H2I *h2i = ref.obj.h2i;
              if (h2i->logz) {
                tpad->SetLogz();
              }
              h2i->th2i->Draw("colz");
              h2i->th2i->GetXaxis()->SetRangeUser(h2i->xmin, h2i->xmax);
              h2i->th2i->GetYaxis()->SetRangeUser(h2i->ymin, h2i->ymax);
            }
            break;
          case Ref::kMultiGraph:
            {
              MultiGraph *mg = ref.obj.mg;
              tpad->SetGridy(mg->gridy);
              mg->tmg->Draw("AP");
            }
            break;
          default:
            abort();
        }
      }

      tc->Update();

      for (auto it2 = c->ref_list.begin(); c->ref_list.end() != it2; ++it2) {
        Ref &ref = *it2;
        switch (ref.type) {
          case Ref::kH1I:
            {
              H1I *h1i = ref.obj.h1i;
              if (!h1i->opt_stat) {
                h1i->th1i->SetStats(0);
              } else {
                ((TPaveStats*)h1i->th1i->FindObject("stats"))->SetOptStat(
                h1i->opt_stat);
              }
            }
            break;
          case Ref::kH2I:
            {
              H2I *h2i = ref.obj.h2i;
              if (!h2i->opt_stat) {
                h2i->th2i->SetStats(0);
              } else {
                ((TPaveStats*)h2i->th2i->FindObject("stats"))->SetOptStat(
                h2i->opt_stat);
              }
            }
            break;
        }
      }
    }

    std::cout << "\rAccumulated events = " << g_neve_monitor << std::flush;
    gSystem->ProcessEvents();

#define SAVE_ALL_PATH "saveall"
    if (0 == stat(SAVE_ALL_PATH, &st)) {
      std::cout << "\nSaving canvases..." << std::endl;
      if (0 != remove(SAVE_ALL_PATH)) {
        std::cerr << my_ctime(&tt) << ":remove(" SAVE_ALL_PATH "): " <<
          strerror(errno) << "." << std::endl;
        std::cerr << "I won't save anything until this is resolved, to not fill up your disk." << std::endl;
        goto cannot_save;
      }
      struct tm *info;
      info = localtime(&tt);
      std::ostringstream oss;
      oss << "online_saved/" << (1900 + info->tm_year) << "-" << (1 +
          info->tm_mon) << "-" << info->tm_mday << "_" << info->tm_hour << "-"
          << info->tm_min << "-" << info->tm_sec;
      std::string dir = oss.str();
      if (0 != gSystem->mkdir(dir.c_str(), kTRUE)) {
        std::cerr << my_ctime(&tt) << ":mkdir(" << dir.c_str() << "): Could "
          "not create directory." << std::endl;
        goto cannot_save;
      }
      int i = 0;
#define SAVE_INFO_PATH "saveinfo"
      std::ofstream ofile(SAVE_INFO_PATH".tmp");
      std::ostringstream oss3;
      oss3 << "montage -mode Concatenate -bordercolor black -border 3 -tile 1";
      for (auto it = g_canvas_map.begin(); g_canvas_map.end() != it; ++it, ++i)
      {
        std::ostringstream oss2;
        std::string s = it->first;
        std::replace(s.begin(), s.end(), ' ', '_');
        std::replace(s.begin(), s.end(), '/', '_');
        oss2 << dir << "/c" << i << "-" << s << ".png";
        std::string path = oss2.str();
        ofile << path << std::endl;
        it->second->tcanvas->SaveAs(path.c_str());
        oss3 << " " << path;
      }
      std::cout << "Done, you can find PNGs in \"" << dir.c_str() << "\"." <<
          std::endl;
      std::ostringstream oss2;
      oss2 << dir << "/montage.png";
      std::string montage_path = oss2.str();
      oss3 << " " << montage_path;
      if (0 != system(oss3.str().c_str())) {
        std::cerr << "Could not create " << montage_path << "!" << std::endl;
      } else {
        std::cerr << "You can also find a figure montage in " << montage_path
            <<"." << std::endl;
      }
      ofile << montage_path << std::endl;
      ofile.close();
      if (0 != rename(SAVE_INFO_PATH".tmp", SAVE_INFO_PATH)) {
        warn("rename(" SAVE_INFO_PATH ".tmp, " SAVE_INFO_PATH ")");
      }
    }
cannot_save:
    ;

    /* Rinse and repeat, unless in toy mode. */
    if (g_is_toyer && is_data_fine) {
      ++g_do_stop;
      break;
    }
  }
  thread->Join();
}

#ifndef __CINT__
# include <TApplication.h>
int main(int argc, char **argv)
{
  g_is_toyer = NULL != strstr(argv[0], "toyer");
  TApplication theApp("App", &argc, argv);
  plotter();
  if (g_is_toyer) {
    printf("\nToyer ready, go nuts!\n");
    theApp.Run();
  }
  return 0;
}
#endif
