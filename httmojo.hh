#ifndef HTT_MOJO_H
#define HTT_MOJO_H

#include "TFile.h"
#include "TH2I.h"
#include "TH2.h"

#include "TArtEventStore.hh"

#include "TArtBigRIPSParameters.hh"
#include "TArtSAMURAIParameters.hh"

#include "TArtCalibNeuLAND.hh"
#include "TArtCalibNeuLANDVETO.hh"
#include "TArtCalibPlastic.hh"

#include <stdint.h>
#include <vector>

class httmojo : public TObject {

public:
  httmojo();
  ~httmojo();

  void Run();

  void InitAnalysis();
  void BookHist();
  void AnalyzeOneEvent();
  void EventLoop();
  void Dump(uint64_t);
  void ClearHist();
  void Delete();

private:
  TFile *fRootFile;
  TArtEventStore* festore;

  TArtBigRIPSParameters* fbripsprm;
  TArtSAMURAIParameters* fsmprm;

  TArtCalibNeuLAND *fCalibNeuLAND;
  TArtCalibNeuLANDVETO *fCalibNeuLANDVETO;
  TArtCalibPlastic *fCalibPlastic;

  Bool_t fInitAnalysis;
  Bool_t fIsHistBooked;
  std::vector<TH2I*> fhist_array;

  TH2I* fhneuland_y_vs_x[8];
  TH2I* fhneuland_tdiff_vs_bar[4];
  TH2I* fhneuland_dthit_vs_bar[4];
  TH2I* fhneuland_q_vs_bar[2];
  TH2I* fhneuland_t_vs_bar[2];
  TH2I* fhneuland_tof_vs_q[8];
  TH2I* fhneuland_tof_vs_q_tot;
  TH2I* fhneuland_tof_vs_bar;
  TH2I* fhneuland_tof_vs_plane;
  TH1I* fhneuland_tof[8];
  TH1I* fhneuland_tof_tot;

  TH2I* fhneulandv_qu_vs_bar;
  TH2I* fhneulandv_qd_vs_bar;
  TH2I* fhneulandv_tu_vs_bar;
  TH2I* fhneulandv_td_vs_bar;

  TH1I* fhsbtt[4];

  UInt_t fvscalers[6];
  UInt_t fvscalerslatch[6];
  std::vector<int32_t> fvrates[6];

  void writeHeader(FILE *, char const *) const;
  void writeString(FILE *, std::string const &) const;
  void writeTCanvas(FILE *, std::string const &, int, int) const;
  void writeTH1I(FILE *, std::string const &, TH1I const *, uint32_t = 0,
      uint8_t = 0) const;
  void writeTH2I(FILE *, std::string const &, TH2I const *, uint32_t = 0,
      uint8_t = 0) const;
  void writeTMultigraph(FILE *, std::string const &, uint8_t) const;
  void writeVec32(FILE *, std::string const &, uint8_t, std::vector<int32_t>
      const *) const;

  ClassDef(httmojo,1);
};

#endif
