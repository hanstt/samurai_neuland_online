#!/bin/sh

identity=
[ -f opt_identity.txt ] && identity=`cat opt_identity.txt`
scp -C $identity s034@ribfana02.riken.jp:/home/s034/exp/exp1706_s034/anaroot/users/neuland/hists.dat tmp.dat > /dev/null 2>&1
[ -f tmp.dat ] || exit 1
siz=`ls -l tmp.dat | awk '{print $5}'`
esiz=`od -N4 -td4 tmp.dat | awk '{print $2}'`
[ $esiz -eq $siz ] && mv tmp.dat hists.dat
