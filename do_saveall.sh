#!/bin/sh

touch saveall
rm -f saveinfo
for i in `seq 1 30`
do
	echo "Waiting for reply..."
	if [ -f saveinfo ]
	then
		echo "Plotter saved the following pictures:"
		cat saveinfo
		rm -f saveinfo
		exit 0
	fi
	sleep 1
done
echo "Plotter did not save any histograms, please check!" 1>&2
exit 1
