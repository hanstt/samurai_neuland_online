#include "httmojo.hh"

#include <arpa/inet.h>
#include <sys/stat.h>
#include <err.h>
#include <cerrno>
#include <cerrno>
#include <csignal>
#include <cstdint>
#include <iostream>
#include <sstream>

#include "TDatime.h"
#include "TClonesArray.h"

#include "TArtStoreManager.hh"
#include "TArtRawEventObject.hh"
#include "TArtRawSegmentObject.hh"
#include "TArtEventInfo.hh"
#include "TArtNeuLANDPla.hh"
#include "TArtNEBULAPla.hh"
#include "TArtPlastic.hh"

#define LENGTH(x) (sizeof x / sizeof *x)

namespace {
double gettime()
{
  struct timespec ts;
  clock_gettime(CLOCK_MONOTONIC, &ts);
  return ts.tv_sec + 1e-9 * ts.tv_nsec;
}
}

//_________________________________________________________________________________
// function to exit loop at keyboard interrupt.
static int stoploop;

void stop_interrupt(int)
{
  printf("\nAha, someone pressed ctrl+c! Gonna quit politely, hang on...\n");
  ++stoploop;
  if (3 <= stoploop) {
    printf("User is spamming ctrl+c, I shall comply and die.\n");
    exit(EXIT_FAILURE);
  }
}
//_________________________________________________________________________________
void httmojo::Run()
{
  if (!fInitAnalysis) InitAnalysis();
  if (!fIsHistBooked) BookHist();
  else                ClearHist();
  EventLoop();
}
//_________________________________________________________________________________
httmojo::httmojo():
    festore(0), 
    fsmprm(0),
    fCalibNeuLAND(0),
    fCalibNeuLANDVETO(0),
    fCalibPlastic(0),
    fInitAnalysis(false), fIsHistBooked(false)
{
  memset(fvscalers, 0, sizeof fvscalers);
  memset(fvscalerslatch, 0, sizeof fvscalerslatch);
  for (int i = 0; 6 > i; ++i) {
    // 5s units, 3600 -> 5 h.
    fvrates[i].resize(3600);
  }
  Run();
}
//_________________________________________________________________________________
httmojo::~httmojo()
{
  Delete();
}
//______________________________________________________________________________
void httmojo::InitAnalysis()
{
  fbripsprm = TArtBigRIPSParameters::Instance();
  fbripsprm->LoadParameter((char *)"db/SAMURAIPlastic.xml");

  fsmprm = TArtSAMURAIParameters::Instance();
  fsmprm->LoadParameter("db/NEULAND.xml");
  fsmprm->LoadParameter("db/NEULANDVETO.xml");
  fsmprm->LoadTCal("db/nnp_tcal_r0525ridf.csv");

  fCalibNeuLAND  = new TArtCalibNeuLAND;
  fCalibNeuLANDVETO  = new TArtCalibNeuLANDVETO;
  fCalibPlastic = new TArtCalibPlastic;

  festore = new TArtEventStore;

  fInitAnalysis = true;
}
//_________________________________________________________________________________
void httmojo::BookHist()
{
  fRootFile = new TFile("httmojo.root","recreate");

  // NeuLAND histograms.
  for (int i = 0; 8 > i; ++i) {
    std::ostringstream oss;
    oss << "hneuland_y_vs_x_" << i;
    std::string name = oss.str();
    oss.str("");
    oss.clear();
    oss << "NeuLAND plane " << (i + 1) << " X-Y";
    std::string caption = oss.str();
    fhneuland_y_vs_x[i] = new TH2I(name.c_str(), caption.c_str(),
        68,-1700,1700, 68,-1700,1700);
    fhist_array.push_back(fhneuland_y_vs_x[i]);
  }
  int range[4] = {40, 40, 300, 300};
  for (int i = 0; 4 > i; ++i) {
    std::ostringstream oss;
    oss << "hneuland_tdiff_vs_bar_" << i;
    std::string name = oss.str();
    oss.str("");
    oss.clear();
    oss << "NeuLAND TDiff " << i;
    std::string caption = oss.str();
    fhneuland_tdiff_vs_bar[i] = new TH2I(name.c_str(), caption.c_str(),
        400,0.5,400.5, 2 * range[i],-range[i],range[i]);
    fhist_array.push_back(fhneuland_tdiff_vs_bar[i]);
  }
  for (int i = 0; 4 > i; ++i) {
    std::ostringstream oss;
    oss << "hneuland_dthit_vs_bar_" << i;
    std::string name = oss.str();
    oss.str("");
    oss.clear();
    oss << "NeuLAND dt-hit " << i;
    std::string caption = oss.str();
    fhneuland_dthit_vs_bar[i] = new TH2I(name.c_str(), caption.c_str(),
        400,0.5,400.5, 100,-10,10);
    fhist_array.push_back(fhneuland_dthit_vs_bar[i]);
  }
  for (int i = 0; 2 > i; ++i) {
    std::ostringstream oss;
    oss << "hneuland_q_vs_bar" << i;
    std::string name = oss.str();
    oss.str("");
    oss.clear();
    oss << "NeuLAND E" << (i + 1) << " vs bar";
    std::string caption = oss.str();
    fhneuland_q_vs_bar[i] = new TH2I(name.c_str(), caption.c_str(),
        400,0.5,400.5, 1000,0,100);
    fhist_array.push_back(fhneuland_q_vs_bar[i]);
  }
  for (int i = 0; 2 > i; ++i) {
    std::ostringstream oss;
    oss << "hneuland_t_vs_bar" << i;
    std::string name = oss.str();
    oss.str("");
    oss.clear();
    oss << "NeuLAND T" << (i + 1) << " vs bar";
    std::string caption = oss.str();
    fhneuland_t_vs_bar[i] = new TH2I(name.c_str(), caption.c_str(),
        400,0.5,400.5, 1000,-2000,2000);
    fhist_array.push_back(fhneuland_t_vs_bar[i]);
  }
  int const c_tof_bin_num = 2500;
  Double_t const c_tof_min = -1000;
  Double_t const c_tof_max = -500;
  for (int i = 0; 8 > i; ++i) {
    std::ostringstream oss;
    oss << "hneuland_tof_vs_q_" << i;
    std::string name = oss.str();
    oss.str("");
    oss.clear();
    oss << "NeuLAND plane " << (i + 1) << " ToF vs E";
    std::string caption = oss.str();
    fhneuland_tof_vs_q[i] = new TH2I(name.c_str(), caption.c_str(),
	       20,0,100, c_tof_bin_num,c_tof_min,c_tof_max);
    fhist_array.push_back(fhneuland_tof_vs_q[i]);
  }
  fhneuland_tof_vs_q_tot = new TH2I("hneuland_tof_vs_q_tot", "NeuLAND ToF vs E",
				    100,0,100, 200,c_tof_min,c_tof_max);
  fhist_array.push_back(fhneuland_tof_vs_q_tot);
  fhneuland_tof_vs_bar = new TH2I("hneuland_tof_vs_bar", "NeuLAND ToF vs bar",
        400,0.5,400.5, 200,c_tof_min,c_tof_max);
  fhist_array.push_back(fhneuland_tof_vs_bar);
  fhneuland_tof_vs_plane = new TH2I("hneuland_tof_vs_plane",
      "NeuLAND ToF vs plane", 8,0.5,8.5, 200,c_tof_min,c_tof_max);
  fhist_array.push_back(fhneuland_tof_vs_plane);
  for (int i = 0; 8 > i; ++i) {
    std::ostringstream oss;
    oss << "hneuland_tof_" << i;
    std::string name = oss.str();
    oss.str("");
    oss.clear();
    oss << "NeuLAND plane " << (i + 1) << " ToF";
    std::string caption = oss.str();
    fhneuland_tof[i] = new TH1I(name.c_str(), caption.c_str(),
200,c_tof_min,c_tof_max);
  }
  fhneuland_tof_tot = new TH1I("hneuland_tof_tot", "NeuLAND ToF total",
      400,c_tof_min,c_tof_max);

  // VETO histograms.
  fhneulandv_qu_vs_bar = new TH2I("hneulandv_qu_vs_bar", "NEULAND VETO id-QUraw", 8,0.5,8.5, 100, 0,4000);
  fhneulandv_qd_vs_bar = new TH2I("hneulandv_qd_vs_bar", "NEULAND VETO id-QDraw", 8,0.5,8.5, 100, 0,4000);
  fhneulandv_tu_vs_bar = new TH2I("hneulandv_tu_vs_bar", "NEULAND VETO id-TUraw", 9,0.5,9.5, 100, 0,4000);
  fhneulandv_td_vs_bar = new TH2I("hneulandv_td_vs_bar", "NEULAND VETO id-TDraw", 9,0.5,9.5, 100, 0,4000);
  fhist_array.push_back(fhneulandv_qu_vs_bar);
  fhist_array.push_back(fhneulandv_qd_vs_bar);
  fhist_array.push_back(fhneulandv_tu_vs_bar);
  fhist_array.push_back(fhneulandv_td_vs_bar);

  // SBT 2 plastics + combined.
  for (int i = 0; 4 > i; ++i) {
    std::ostringstream oss;
    oss << "hsbtt_" << i;
    std::string name = oss.str();
    oss.str("");
    oss.clear();
    switch (i) {
      case 0:
      case 1:
        oss << "SBT " << (i + 1) << " t";
        break;
      case 2:
        oss << "SBT combi t";
        break;
      case 3:
        oss << "SBT diff t";
        break;
    }
    std::string caption = oss.str();
    fhsbtt[i] = new TH1I(name.c_str(), caption.c_str(), 400,3==i?-1000:-200,1000);
  }

  fIsHistBooked = true;
}
//_________________________________________________________________________________
void httmojo::AnalyzeOneEvent()
{
  if (festore->GetNextEvent()) {

    //------------------------------------------
    // T0
    fCalibPlastic->ClearData();
    fCalibPlastic->ReconstructData();
    TArtPlastic *sbt1 = (TArtPlastic *)fCalibPlastic->FindPlastic((char *)"F13pl-1");
    Double_t sbt1_t = sbt1 ? sbt1->GetTime() : 0.0;
    fhsbtt[0]->Fill(sbt1_t);
    TArtPlastic *sbt2 = (TArtPlastic *)fCalibPlastic->FindPlastic((char *)"F13pl-2");
    Double_t sbt2_t = sbt2 ? sbt2->GetTime() : 0.0;
    fhsbtt[1]->Fill(sbt2_t);
    Double_t denom = sbt1_t && sbt2_t ? 2.0 : 1.0;
    Double_t sbtc_t = (sbt1_t + sbt2_t) / denom;
    fhsbtt[2]->Fill(sbtc_t);
    Double_t sbtd_t = sbt1_t - sbt2_t;
    fhsbtt[3]->Fill(sbtd_t);

/*    Double_t sbt1_t, sbt2_t;
    TClonesArray *pla_array = fCalibPlastic->GetPlasticArray();
    Int_t npla = pla_array->GetEntries();
    for (int ipla=0;ipla<npla;++ipla){
      TArtPlastic *pla = (TArtPlastic*)pla_array->At(ipla);
      if (pla->GetID()==4) sbt1_t = pla->GetTime();
      if (pla->GetID()==5) sbt2_t = pla->GetTime();
    }
    fhsbtt[0]->Fill(sbt1_t);
    fhsbtt[1]->Fill(sbt2_t);
    Double_t denom = sbt1_t && sbt2_t ? 2.0 : 1.0;
    Double_t sbtc_t = (sbt1_t + sbt2_t) / denom;
    fhsbtt[2]->Fill(sbtc_t);
    Double_t sbtd_t = sbt1_t - sbt2_t;
    fhsbtt[3]->Fill(sbtd_t);*/

    //------------------------------------------
    // neuland veto
    fCalibNeuLANDVETO->ClearData();
    fCalibNeuLANDVETO->ReconstructData();
    Int_t NumPla = fCalibNeuLANDVETO->GetNumNeuLANDVETOPla();
    Bool_t has_veto = false;
    for(int i=0; i < NumPla; ++i){
      TArtNEBULAPla* pla = fCalibNeuLANDVETO->GetNeuLANDVETOPla(i);
      Int_t ID=pla->GetID();
      if (ID<9) {
        fhneulandv_qu_vs_bar->Fill(ID, pla->GetQURaw());
        fhneulandv_qd_vs_bar->Fill(ID, pla->GetQDRaw());
      }
      if (pla->GetQUCal() > 3 || pla->GetQDCal() > 3) {
        has_veto = true;
      }
      fhneulandv_tu_vs_bar->Fill(ID, pla->GetTURaw());
      fhneulandv_td_vs_bar->Fill(ID, pla->GetTDRaw());
    }

    //------------------------------------------
    // scalers
    TArtStoreManager *sman = TArtStoreManager::Instance();
    TArtRawEventObject *raw_event = festore->GetRawEventObject();
    for (int i = 0; raw_event->GetNumSeg() > i; ++i) {
      TArtRawSegmentObject *seg = raw_event->GetSegment(i);
      // Hmm, let's hope no other scaler has 6 channels...
      if (13 == seg->GetFP() &&
          63 == seg->GetDetector() &&
          8 == seg->GetModule() &&
          24 == seg->GetDataSize()) {
        for (int j = 0; 6 > j; ++j) {
          fvscalers[j] = ntohl(seg->GetData(j)->GetVal());
        }
      }
    }

    //------------------------------------------
    // neuland
    fCalibNeuLAND->ClearData();
    fCalibNeuLAND->ReconstructData();

    Int_t nneuland = fCalibNeuLAND->GetNumNeuLANDPla();
    int const c_ref[4] = {25, 75, 175, 225};
    Double_t t_ref[4] = {-1, -1, -1, -1};
    for(int i=0;i<nneuland;i++) {
      TArtNeuLANDPla *pla = fCalibNeuLAND->GetNeuLANDPla(i);
      if (!pla->GetBothFired()) {
        continue;
      }
      Double_t t_hit = (pla->GetTCal(0) + pla->GetTCal(1)) / 2;
      for (int ref_i = 0; 4 > ref_i; ++ref_i) {
        if (c_ref[ref_i] == pla->GetID()) {
          t_ref[ref_i] = t_hit;
        }
      }
    }
    for(int i=0;i<nneuland;i++) {
      TArtNeuLANDPla *pla = fCalibNeuLAND->GetNeuLANDPla(i);
      Int_t ID=pla->GetID();
    
      fhneuland_y_vs_x[pla->GetLayer()]->Fill(pla->GetX(), pla->GetY());
      Double_t tdiff = pla->GetTCal(1) - pla->GetTCal(0);
      Double_t t_hit = (pla->GetTCal(0) + pla->GetTCal(1)) / 2;
      fhneuland_tdiff_vs_bar[0]->Fill(ID, tdiff);
      fhneuland_tdiff_vs_bar[2]->Fill(ID, tdiff);
      if (!has_veto) {
        fhneuland_tdiff_vs_bar[1]->Fill(ID, tdiff);
        fhneuland_tdiff_vs_bar[3]->Fill(ID, tdiff);
      }
      if (pla->GetBothFired()) {
        for (int ref_i = 0; 4 > ref_i; ++ref_i) {
          if (c_ref[ref_i] == ID || 0 > t_ref[ref_i]) {
            continue;
          }
          Double_t dt = t_hit - t_ref[ref_i];
          fhneuland_dthit_vs_bar[ref_i]->Fill(ID, dt);
        }
      }
      for (int i = 0; 2 > i; ++i) {
        fhneuland_q_vs_bar[i]->Fill(ID, pla->GetQCal(i));
      }
      for (int i = 0; 2 > i; ++i) {
        fhneuland_t_vs_bar[i]->Fill(ID, pla->GetTCal(i));
      }
      Double_t q = sqrt(pla->GetQCal(0) * pla->GetQCal(1));
      Double_t ToF = 0.5 * (pla->GetTCal(0) + pla->GetTCal(1)) - sbtc_t;
      fhneuland_tof_vs_q[pla->GetLayer()]->Fill(q, ToF);
      fhneuland_tof_vs_q_tot->Fill(q, ToF);
      fhneuland_tof_vs_bar->Fill(ID, ToF);
      fhneuland_tof_vs_plane->Fill(pla->GetLayer() + 1, ToF);
      fhneuland_tof[pla->GetLayer()]->Fill(ToF);
      fhneuland_tof_tot->Fill(ToF);
    }
    festore->ClearData();
  }
}
//______________________________________________________________________________
void httmojo::EventLoop()
{
  signal(SIGINT, stop_interrupt);
  //festore->Open("/home/tm1510/ridf/sdaq02/NeuLAND_NEBULA0002.ridf"); // Data from old file for testing
  //festore->Open("ridf/sdaq02/run0050.ridf"); // Data from old file for testing
  //festore->Open("/home/s027/ridf/sdaq02/run0099.ridf"); // Data from old file for testing
  festore->Open(0); // Real data

  double time_watchdog;
  double time_dump;
  double time_rate;
  ULong64_t neve = 0;
  ULong64_t neve_monitor = 0;
  ULong64_t neve_monitor_prev = 0;
  Double_t neve_rate = 0;
  for (stoploop = 0; 0 == stoploop;) {
    double time_cur = gettime();

    AnalyzeOneEvent();

    double d = time_cur - time_rate;
    if (d > 5) {
      bool zero = true;
      for (int i = 0; 6 > i; ++i) {
        if (fvscalerslatch[i]) {
          zero = false;
          break;
        }
      }
      if (zero) {
        memmove(fvscalerslatch, fvscalers, sizeof fvscalerslatch);
      }
      /* Moving scaler diffs. */
      for (int i = 0; 6 > i; ++i) {
        memmove(&fvrates[i].at(0), &fvrates[i].at(1), sizeof fvrates[i].at(0)
            * (fvrates[i].size() - 1));
        fvrates[i].at(fvrates[i].size() - 1) = fvscalers[i] -
            fvscalerslatch[i];
        fvscalerslatch[i] = fvscalers[i];
      }
      /* Scale with 1 kHz clock. */
      int khz = fvrates[5].at(fvrates[5].size() - 1);
      for (int i = 0; 6 > i; ++i) {
        int diff = fvrates[i].at(fvrates[i].size() - 1);
        diff = (1000ULL * diff) / (khz ? khz : 1000);
        fvrates[i].at(fvrates[i].size() - 1) = diff;
      }
      time_rate = time_cur;
    }

    ++neve;
    ++neve_monitor;

    if (time_cur - time_watchdog > 1){
      std::cout << "\rWatchdog says: " << neve_monitor << '/' << neve <<
          " events in histos/total (ctrl+c to stop)." << std::flush;
      time_watchdog = time_cur;
    }
#define REDRAW_LIMIT 1000
    Bool_t do_dump = false;
    if (neve_monitor - neve_monitor_prev > REDRAW_LIMIT) {
      neve_monitor_prev = neve_monitor;
      do_dump = true;
    }
    if (time_cur - time_dump > 5) {
      do_dump = true;
    }
    if (do_dump) {
      neve_rate = (Double_t)neve/(time_cur - time_dump);

      Dump(neve_monitor);
      fRootFile->Write();

#define CLEAR_LIMIT 100000
      if (neve_monitor > CLEAR_LIMIT) {
        ClearHist();
        neve_monitor_prev = neve_monitor = 0;
      }
      time_dump = gettime();
    }
  }
}
//______________________________________________________________________________
void httmojo::Dump(uint64_t neve_monitor)
{
#define TMP_FILE "tmp.dat"
#define HIST_FILE "hists.dat"
  FILE *file;
  file = fopen(TMP_FILE, "wb");
  if (NULL) {
    fprintf(stderr, "fopen(" TMP_FILE "): %s.\n", strerror(errno));
    return;
  }
  uint32_t totsize = 0;
  fwrite(&totsize, sizeof totsize, 1, file);
  uint32_t time_cur_s = time(NULL);
  fwrite(&time_cur_s, sizeof time_cur_s, 1, file);
  fwrite(&neve_monitor, sizeof neve_monitor, 1, file);
  writeTCanvas(file, "Y vs X", 2, 4);
  for (int i = 0; 8 > i; ++i) {
    std::ostringstream oss;
    oss << "Plane " << 1 + i;
    writeTH2I(file, oss.str(), fhneuland_y_vs_x[i], 1001111);
  }
  writeTCanvas(file, "T-diff vs bar", 2, 2);
  for (int i = 0; 4 > i; ++i) {
    std::ostringstream oss;
    int const c_range[] = {40, 40, 300, 300};
    oss << "T-diff vs bar (+-" << c_range[i] << ")" << (1 == (1 & i) ?
        " (!veto)" : "");
    writeTH2I(file, oss.str(), fhneuland_tdiff_vs_bar[i]);
  }
  writeTCanvas(file, "dT-hit vs bar", 2, 2);
  for (int i = 0; 4 > i; ++i) {
    int const c_ref[] = {25, 75, 175, 225};
    std::ostringstream oss;
    oss << "dt-hit vs bar, ref=" << c_ref[i];
    writeTH2I(file, oss.str(), fhneuland_dthit_vs_bar[i]);
  }
  writeTCanvas(file, "Q vs bar", 2, 1);
  for (int i = 0; 2 > i; ++i) {
    std::ostringstream oss;
    oss << "Q " << (i + 1) << " vs bar";
    writeTH2I(file, oss.str(), fhneuland_q_vs_bar[i]);
  }
  writeTCanvas(file, "T vs bar", 2, 1);
  for (int i = 0; 2 > i; ++i) {
    std::ostringstream oss;
    oss << "T " << (i + 1) << " vs bar";
    writeTH2I(file, oss.str(), fhneuland_t_vs_bar[i]);
  }
  writeTCanvas(file, "ToF vs Q", 2, 4);
  for (int i = 0; 8 > i; ++i) {
    std::ostringstream oss;
    oss << "Plane " << (i + 1) << " ToF vs Q";
    writeTH2I(file, oss.str(), fhneuland_tof_vs_q[i], 1001111);
  }
  writeTCanvas(file, "ToF vs the world", 1, 3);
  {
    std::ostringstream oss;
    oss << "ToF vs Q total";
    writeTH2I(file, oss.str(), fhneuland_tof_vs_q_tot, 1001111, 1);
  }
  {
    std::ostringstream oss;
    oss << "ToF vs bar";
    writeTH2I(file, oss.str(), fhneuland_tof_vs_bar, 1001111, 1);
  }
  {
    std::ostringstream oss;
    oss << "ToF vs plane";
    writeTH2I(file, oss.str(), fhneuland_tof_vs_plane, 1001111, 1);
  }
  writeTCanvas(file, "ToF/plane", 2, 4);
  for (int i = 0; 8 > i; ++i) {
    std::ostringstream oss;
    oss << "ToF in plane" << (i + 1);
    writeTH1I(file, oss.str(), fhneuland_tof[i], 1001111, 1);
  }
  writeTCanvas(file, "ToF sum", 1, 1);
  {
    std::ostringstream oss;
    oss << "ToF sum";
    writeTH1I(file, oss.str(), fhneuland_tof_tot, 1001111, 1);
  }
  writeTCanvas(file, "Veto Q vs bar", 2, 2);
  {
    std::ostringstream oss;
    oss << "Veto Qup vs bar";
    writeTH2I(file, oss.str(), fhneulandv_qu_vs_bar, 1001111);
  }
  {
    std::ostringstream oss;
    oss << "Veto Qdown vs bar";
    writeTH2I(file, oss.str(), fhneulandv_qd_vs_bar, 1001111);
  }
  {
    std::ostringstream oss;
    oss << "Veto Tup vs bar";
    writeTH2I(file, oss.str(), fhneulandv_tu_vs_bar, 1001111);
  }
  {
    std::ostringstream oss;
    oss << "Veto Tdown vs bar";
    writeTH2I(file, oss.str(), fhneulandv_td_vs_bar, 1001111);
  }
  writeTCanvas(file, "Start/stop", 2, 2);
  for (int i = 0; 4 > i; ++i) {
    std::ostringstream oss;
    switch (i) {
      case 0: /* FALLTHROUGH */
      case 1: oss << "sbtt" << i; break;
      case 2: oss << "sbtct"; break;
      case 3: oss << "sbtdt"; break;
    }
    writeTH1I(file, oss.str(), fhsbtt[i], 1001111, 1);
  }
  writeTCanvas(file, "Scaler rates", 3, 1);
  for (int i = 0; 6 > i; ++i) {
    std::ostringstream oss;
    if (0 == (1 & i)) {
      oss << "tmg" << i;
      writeTMultigraph(file, oss.str(), 1);
    }
    oss.str("");
    oss.clear();
    switch (i) {
      case 0: oss << "Incoming"; break;
      case 1: oss << "1.1 kHz"; break;
      case 2: oss << "NeuLAND m1 LANIIC3"; break;
      case 3: oss << "NeuLAND m1 LANIIC6"; break;
      case 4: oss << "NeuLAND m2 LANIIC3"; break;
      case 5: oss << "NeuLAND m2 LANIIC6"; break;
    }
    int c_map[] = {0, 5, 1, 3, 2, 4};
    writeVec32(file, oss.str(), 2 + 2 * (1 & i), &fvrates[c_map[i]]);
  }
  totsize = ftell(file);
  fseek(file, 0, SEEK_SET);
  fwrite(&totsize, sizeof totsize, 1, file);
  fclose(file);
  if (0 != rename(TMP_FILE, HIST_FILE)) {
    warn("rename("TMP_FILE","HIST_FILE")");
  }
}
//_________________________________________________________________________________
void httmojo::ClearHist()
{
  Int_t nhist = fhist_array.size();
  for(Int_t ihist=0;ihist<nhist;++ihist){
    TH2* hist = fhist_array[ihist];
    hist->Reset("M");
  }
  for(Int_t ihist=0;ihist<8;++ihist){
    fhneuland_tof[ihist]->Reset("M");
  }
  fhneuland_tof_tot->Reset("M");
  for(Int_t ihist=0;ihist<4;++ihist){
    fhsbtt[ihist]->Reset("M");
  }
}
//______________________________________________________________________________
void httmojo::Delete()
{
  delete festore; festore = 0;
  delete fsmprm; fsmprm = 0;
  delete fCalibNeuLAND; fCalibNeuLAND = 0;
  delete fCalibPlastic; fCalibPlastic = 0;
  fInitAnalysis  = false;
}

void httmojo::writeHeader(FILE *a_file, char const *a_header) const
{
  assert(4 == strlen(a_header));
  fputc(a_header[0], a_file);
  fputc(a_header[1], a_file);
  fputc(a_header[2], a_file);
  fputc(a_header[3], a_file);
}

void httmojo::writeString(FILE *a_file, std::string const &a_str) const
{
  fputc(a_str.length(), a_file);
  fprintf(a_file, "%s", a_str.c_str());
}

void httmojo::writeTCanvas(FILE *a_file, std::string const &a_title, int
    a_row_num, int a_col_num) const
{
  writeHeader(a_file, "TCvs");
  writeString(a_file, a_title);
  fputc(a_row_num, a_file);
  fputc(a_col_num, a_file);
}

void httmojo::writeTH1I(FILE *a_file, std::string const &a_title, TH1I const
    *a_th1i, uint32_t a_opt_stat, uint8_t a_logy) const
{
  writeHeader(a_file, "TH1I");

  writeString(a_file, a_title);

  int16_t nx = a_th1i->GetNbinsX();
  double xmin = a_th1i->GetXaxis()->GetXmin();
  double xmax = a_th1i->GetXaxis()->GetXmax();
  fwrite(&nx, sizeof nx, 1, a_file);
  fwrite(&xmin, sizeof xmin, 1, a_file);
  fwrite(&xmax, sizeof xmax, 1, a_file);

  fputc(a_logy, a_file);

  double nentries = a_th1i->GetEntries();
  fwrite(&nentries, sizeof nentries, 1, a_file);
  double stats[4];
  a_th1i->GetStats(stats);
  fwrite(stats, sizeof *stats, LENGTH(stats), a_file);
  fwrite(&a_opt_stat, sizeof a_opt_stat, 1, a_file);
  int32_t const *arr = a_th1i->GetArray();
  fwrite(arr, sizeof *arr, 1 + nx + 1, a_file);
}

void httmojo::writeTH2I(FILE *a_file, std::string const &a_title, TH2I const
    *a_th2i, uint32_t a_opt_stat, uint8_t a_logz) const
{
  writeHeader(a_file, "TH2I");

  writeString(a_file, a_title);

  int16_t nx = a_th2i->GetNbinsX();
  double xmin = a_th2i->GetXaxis()->GetXmin();
  double xmax = a_th2i->GetXaxis()->GetXmax();
  fwrite(&nx, sizeof nx, 1, a_file);
  fwrite(&xmin, sizeof xmin, 1, a_file);
  fwrite(&xmax, sizeof xmax, 1, a_file);

  int16_t ny = a_th2i->GetNbinsY();
  double ymin = a_th2i->GetYaxis()->GetXmin();
  double ymax = a_th2i->GetYaxis()->GetXmax();
  fwrite(&ny, sizeof ny, 1, a_file);
  fwrite(&ymin, sizeof ymin, 1, a_file);
  fwrite(&ymax, sizeof ymax, 1, a_file);

  fputc(a_logz, a_file);

  double nentries = a_th2i->GetEntries();
  fwrite(&nentries, sizeof nentries, 1, a_file);
  double stats[7];
  a_th2i->GetStats(stats);
  fwrite(stats, sizeof *stats, LENGTH(stats), a_file);
  fwrite(&a_opt_stat, sizeof a_opt_stat, 1, a_file);
  int32_t const *arr = a_th2i->GetArray();
  fwrite(arr, sizeof *arr, (1 + nx + 1) * (1 + ny + 1), a_file);
}

void httmojo::writeTMultigraph(FILE *a_file, std::string const &a_title,
    uint8_t a_gridy) const
{
  writeHeader(a_file, "TMGp");
  writeString(a_file, a_title);
  fputc(a_gridy, a_file);
}

void httmojo::writeVec32(FILE *a_file, std::string const &a_title, uint8_t
    a_color, std::vector<int32_t> const *a_vec) const
{
  writeHeader(a_file, "Vc32");
  writeString(a_file, a_title);
  fputc(a_color, a_file);
  uint32_t num = a_vec->size();
  fwrite(&num, sizeof num, 1, a_file);
  fwrite(&a_vec->at(0), sizeof a_vec->at(0), num, a_file);
}

#ifndef __CINT__
int main()
{
  httmojo mojo;
  return 0;
}
#endif
