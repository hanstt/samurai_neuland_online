#!/bin/sh

cd $TARTSYS/..
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$TARTSYS/lib
while true
do
	echo `date`: HTT mojo NeuLAND Online Monitor server starting...
	./samurai_neuland_online/httmojo -b
	echo `date`: HTT mojo NeuLAND Online Monitor server stopped.
	sleep 1
done
